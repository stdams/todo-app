import React, {Component} from 'react';
import Todos from './components/Todos'
import AddTodo from './components/AddTodo'
import {connect} from 'react-redux'
import {deleteTodo, addTodo} from './store/actions/actions'
import {firestoreConnect} from 'react-redux-firebase'
import {compose} from 'redux' 
 

class App extends Component {
  

  deleteTodo = (id) => {
    this.props.deleteTodo(id)
  }

  addTodo = (todo) => {
   this.props.addTodo(todo)
  }
  

  render () {
  return (
    <div className="todo-app container">
      <h1 className="center blue-text">My Todo Listing</h1>
      <Todos todos={this.props.todos} deleteTodo={this.deleteTodo}/>
      <AddTodo addTodo={this.addTodo}/>
    </div>
  );
}}

const mapStateToProps = (state) => {
  return {
    //todos: state.add.todos,
    todos: state.firestore.ordered.todos
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteTodo: (id) => {dispatch(deleteTodo(id))},
    addTodo: (todo) => {dispatch(addTodo(todo))}
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect([
    {collection: 'todos'}
  ])
)(App);
