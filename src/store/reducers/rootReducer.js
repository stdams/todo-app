import addReducer from './addReducer'
import {combineReducers} from 'redux'
import { firestoreReducer } from 'redux-firestore'

const rootReducer = combineReducers({
    add: addReducer,
    firestore: firestoreReducer
    
});

export default rootReducer