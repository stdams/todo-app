const initState = {
    todos: [
        {id: '1', content: 'buy milk'},
        {id: '2', content: 'visit bae'}
    ]
}


const addReducer = (state= initState, action) => {
    if (action.type === 'DELETE_TODO') {
        let newTodos = state.todos.filter(todo => {
            return action.id !== todo.id
        });
        return {
            ...state,
            todos: newTodos
        }
    } else
    if (action.type === 'ADD_TODO') {
        console.log('new todo created')
        return state
        //let todo = action.todo
        //todo.id = Math.random()
        //let newTodos = [...state.todos, todo]
        //return {
        //    ...state,
        //    todos: newTodos
        //}

    } else 
    if (action.type === 'ADD_TODO_ERR' ) {
        console.log('add todo error', action.err)
        return state
    } else 
    if (action.type === 'DELETE_TODO') {
        console.log('deleted todo')
        return state
    } else 
    if (action.type === 'DELETE_TODO_ERR') {
        console.log('deleting error', action.err)
        return state
    }
    return state
}

export default addReducer