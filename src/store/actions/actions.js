export const deleteTodo = (id) => {
    return (dispatch, getState, {getFirestore, getFirebase}) => {
        //make async call to database
        const firestore= getFirestore();
        firestore.collection("todos").doc(id).delete().then(() => {
            dispatch({type: 'DELETE_TODO',id});
        }).catch((err) => {
            dispatch({type: 'DELETE_TODO_ERR', err})
        })
        
        
    
    }
}

export const addTodo = (todo) => {
    return (dispatch, getState, {getFirestore, getFirebase}) => {
        //make async call to database
        const firestore = getFirestore();
        firestore.collection("todos").add({
            ...todo, 
        }).then(() => {
            dispatch({ type: 'ADD_TODO', todo });
        }).catch((err) => {
            dispatch({type: 'ADD_TODO_ERR', err})
        })
    }
} 