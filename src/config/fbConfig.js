import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

  var firebaseConfig = {
    apiKey: "AIzaSyDCsZEblCFb3fEn0nnQdougv51PF6K-jGo",
    authDomain: "my-todo-app-58189.firebaseapp.com",
    databaseURL: "https://my-todo-app-58189.firebaseio.com",
    projectId: "my-todo-app-58189",
    storageBucket: "my-todo-app-58189.appspot.com",
    messagingSenderId: "236482079746",
    appId: "1:236482079746:web:5a6c05de8a7eab41af4b1b",
    measurementId: "G-EVFM9JQQX1"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  //firebase.analytics();
  //firebase.firestore().settings({ timestampsInSnapshots: true })

  export default firebase;